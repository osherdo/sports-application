<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routine extends Model
{
  public function __construct()
{
   $fillable = ['id,user_id,routine_name,routine_type'];
}

 public function user()
{
  return $this->belongsTo('App\User');
}


}
