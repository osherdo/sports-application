<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Validator;
use Response;
use Auth;
use App\ProfileImage;

class DropzoneController extends Controller
{

public function uploadFiles(Request $request)
{
  $message="Profile image  uploaded.";
  $input = $request->all();
  $user = auth()->user();
  $rules = array
  (
      'file' => 'image|max:3000',
  );

  $validation = Validator::make($input, $rules);

  if ($validation->fails())
  {
      return Response::make($validation->errors->first(), 400);
  }

  $destinationPath = 'uploads/'.$user->username; // upload path: public/uploads
  $extension = $request->file('file')->getClientOriginalExtension(); // getting file extension
  $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
  $upload_success = Input::file('file')->move($destinationPath, $fileName); // uploading file to given path

  if ($upload_success)
  {
      $profile_image= ProfileImage::create(['user_id'=>$user->id, 'file_name'=>$fileName]);
      $user->recent_profile_iamge= $fileName;
      $user->save();
      return Response::json('success', 200);
  }
  else
  {
      return Response::json('error', 400);
  }

//  attaching the profile image to the authenticated user.

$user->profileImage()->create([
    'filename' => $filename
]);

Session::flash('new_profile_image', 'Profile Photo Updated!');
}

public function authorize($ability, $arguments = []) // Set default value to be an empty array (in the second argument.).
// [] is allowed to use since php 5.4
{
      // set to true instead of false
      return true;
}

private function resize($image, $size)
  {
    try
    {
      $extension 		= 	$image->getClientOriginalExtension();
      $imageRealPath 	= 	$image->getRealPath();
      $thumbName 		= 	'thumb_'. $image->getClientOriginalName();

      //$imageManager = new ImageManager(); // use this if you don't want facade style code
      //$img = $imageManager->make($imageRealPath);

      $img = Image::make($imageRealPath); // use this if you want facade style code
      $img->resize(intval($size), null, function($constraint) {
         $constraint->aspectRatio();
      });
      return $img->save(public_path('images/user_images'). '/'. $thumbName);
    }
    catch(Exception $e)
    {
      return false;
    }

  }



}
