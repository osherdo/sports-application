<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class UserProfilesController extends Controller
{
    protected function store(Request $request) //using request object.
    {
        $user = Auth::user();

        $today = strtotime(date("Y-m-d")); // Convert the current datetime to an integer value.
        $dob = strtotime($request['age']); // Getting the selected value from date picker.

        $diff_years = floor(abs($today-$dob)/(365*60*60*24)); // returning number of seconds(unix timestamp),down-rounded,absolute number of the seconds that are differncing between current time and birth date.
        // Then dividing it with the number of seconds in a year, gives an integer that's an asbsolute number representing the age.

        //echo $diff_years; die;
        // Flashing error message to the session.
        if ($diff_years < 16) {
            \Session::flash('error', 'You should be at least 16 years old.');

            return back();
        }

        // A profile belongs to a user
        $profile = $user->profile()->create([
            'name' => $request['name'],
            'age' => $diff_years,
            'goals' => $request['goals'],
            'activityType' => $request['activityType'],
            'gender' => $request['gender']
        ]);

        $profile->expectations()->attach($request->expectations);

        return redirect('hub');  // this is where you redirect to the hub after you store the User.
    }

    public function showProfile()
    {
        $user= auth()->user();
        if($user)
        {
           return view('auth.profile' , compact('user'));
        }
        else
        {
            // Return flash error message.
            return redirect('auth/login')->with('login_needed', 'Your session has timed-out. Please login again.');

        }
    }


}
