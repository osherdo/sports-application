<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileImage extends Model
{
    protected $table="profile_images";
    protected $fillable=["user_id", "file_name"];


    public function user()
    {
      return $this->belongsTo(User::class);
    }

    public function getPathAttribute()
{
    return $this->filePath;
}

}
