$(".remove_routine").click(function () { // for each routine delete button. 
    var self = $(this); // Create a jquery object of the button clicked.
    var token = $(this).attr("data-token"); // Fetches the token from the data-token attribute of the button pressed.

    var id = $(this).attr("data-id"); // Getting the data-id attribute content of the routine where the delete button has been clicked

    
    var data = {
        "_token": token, //taken from token variable above.
        "id" : id //taken from id variable above.
    };
    //First argument: url to send ajax request to.
    //
    $.post("/routine/delete", data, function (val) {
        if (val.status == "success") {
            self.closest("div.exercise_row").remove(); // removing the picked routine to be deleted, and it's determined with the self variable.
        }
    });
});
