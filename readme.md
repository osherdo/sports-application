Welcome to my sports application! I am Osher, and I am a junior full-stack web developer.

The app is written using Laravel Framework (5.1).

Currently you can: 
* register
* login
* search for other users
* post updates on your profile
* follow and unfollow users
* create workout routine
* Modify and delete existing personal routine.

More coming up in the future!