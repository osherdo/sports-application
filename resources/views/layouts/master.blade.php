<!DOCTYPE html>
<html lang="en">
<head>
   <!--For Bootstrap mobile proper rendering and touch zooming -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
 <meta charset="utf-8">
 <meta name="_token" content="{{ csrf_token() }}"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <script src="/js/dropzone.js"></script>
  <script src="/js/content-filter/modernizr.js"></script>
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/base-min.css">

  <!--Use the following value to display the webpage in edge mode, which is the highest standards mode supported by Internet Explorer, from Internet Explorer 6 through IE11. -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 <script src="/js/jquery-ui.min.js"></script>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">

    <!-- <link rel="stylesheet" href="/css/app.css"/> -->

    @yield('scripts')
    <title>Sports App @yield('title')</title>

</head>

<body>

@section('navigation')
<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/hub">Sports-App</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">

                    <li>
                        <a href="/view_routine">My Routines</a>
                    </li>
                   <li>
                        <a href="/routine">New Routine</a>
                    </li>

                    <li>
                        <a href="/hub">Hub</a>
                    </li>

                    <li>
                        <a href="/auth/logout">Logout</a>
                    </li>

                    <li>
                        <a href="/profile">Profile Images</a>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
@endsection
@yield('navigation')

    @section('sidebar')
    <div class="container">
      <br>
      <div class="row">
<div class="col-md-3  col-sm-4">
 <form action="{{ route('search') }}" method="post" class="form-inline">
  <!--the form action is the method in a route called search -->
  {!! csrf_field() !!}
    <div class="form-group">
    <input type="text" placeholder="Search for name/user..." class="form-control" required name="NameUser">
  </div>

    <input type="submit" name="searchButton" value="Search" class="btn btn-warning">
 </form>
</div>
<div class="col-md-6 col-sm-8">
  <div class="row">
    <div class="col-md-1">Or:</div>
    <div class="col-md-11">
      <!-- End of responsive search. -->
      {!! Form::open(array('route'=>'multi_search'))!!}
      {!! csrf_field() !!}

  <div class="second_select">
    <div class="form-group">
   <!-- preferences search: -->
   <select id="select_preferences" name="select_preferences[]" multiple="multiple"> <!-- using [] to return multiple options and make the select an array. -->

    @if(isset($expectations_list))
    @foreach($expectations_list as $exp)
    <option value="{{$exp->id}}" class="options">{{$exp->name}}</option>
    @endforeach
   @endif
   </select>
  </div>
  <!-- html code for age-range selector -->

  <div class="form-group">
   <div id="range-div" class="hide">
   </div>
 </div>
 <div class="form-group">
   <div id="range-options" class="hide-2">
    <label for="amount">within the ages:</label>
    <input id="amount" name="amount" class="form-control"> <!-- Getting the ages to later be used in the controller (using the name attribute) -->


   </div>
   </div>
   <div class="form-group">
   <div id="range-options" class="hide-2">
    <input type="submit" name="2ndsearchButton" id="submitMultiSearch" value="Search" class="btn btn-warning">
   </div>
   </div>
  </div>

{!! Form::close()!!}
    </div>
  </div>
</div>
</div>
        @yield('content')

</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="{{asset('js/bootstrap-multiselect.js')}}"></script>

    <!-- Script for the multi-select. -->

    <script>
    $(document).ready(function() {

    $('#select_preferences').multiselect({
        buttonText: function(options, select) {
            return 'Look for users that:';
        },
        buttonTitle: function(options, select) {
            var labels = [];
            options.each(function() {
                labels.push($(this).text()); // get the options in a string (with .text method). later it would be joined with - separated between each.
            });
            $('#range-div,#range-options').toggleClass('hide', labels.length === 0); // hiding both range-div and range-options divs.

             // The class 'hide' hide the content.
             //So I remove it so it would be visible.
             //If the labels array is empty - then do use the hide class to hide the range-selector
            return labels.join(' - ');
 // the options seperated by ' - '
        // This returns the text of th options in labels[].
        }
    });

});
  </script>

<!-- Jquery's range slider script -->
  <script>
  $(function() {
    $( "#range-div" ).slider({
      range: true,
      min: 16,
      max: 120,
      values: [ 16, 45 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val($( "#range-div" ).slider( "values", 0 ) +
      " - " + $( "#range-div" ).slider( "values", 1 ) );
  });
  </script>
  <!-- Script for showing & hiding the top-bar. -->
  <script type="text/javascript">

  // Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('nav.navbar').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function()
{
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop(); // Getting the current vertical value for the scrolling (offset). Getting the current number in pixels that's getting the current distance in px from the top.
    //console.log(st); // See the current distance in px from the top.
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return; //This on-demand exit if the condition is positive then we just leave the function with an empty result.

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('nav.navbar').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('nav.navbar').removeClass('nav-up').addClass('nav-down');
        }
    }

    lastScrollTop = st; //Compare current scroll to how much we got down on the page.
}

  </script>
</body>
@yield('footer')

</html>
