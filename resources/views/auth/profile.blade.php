@extends('layouts.master')

@section('content')
	<div class="">
      <div class="row">
          <label for="">Your profile image:</label>
          <image src="/uploads/{{ $user->username."/".$user->recent_profile_iamge }}" width="100" heigth="100"/>
      </div>
<br>
    <label for="">All Profile Images:</label>
     <div class="col-md-12">
       @foreach($user->profileImages as $image)
        <div class="col-md-3" style="padding:10px;">
          <image src="/uploads/{{ $user->username."/".$image->file_name }}" width="300" height="300"/>
        </div>

				@if (session('login_needed'))
    <div class="alert alert-warning">
        {{ session('login_needed') }}
    </div>
@endif
       @endforeach
     </div>
	</div>
@stop
