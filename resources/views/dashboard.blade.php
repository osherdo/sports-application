<!DOCTYPE html>
<html lang="en">
<head>
   <!--For Bootstrap mobile proper rendering and touch zooming -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
 <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--Use the following value to display the webpage in edge mode, which is the highest standards mode supported by Internet Explorer, from Internet Explorer 6 through IE11. -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 <script src="/js/jquery-ui.min.js"></script>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}" >

    <link rel="stylesheet" href="/assets/datepicker/css/bootstrap-datepicker.min.css" />
    <script src="/assets/datepicker/js/bootstrap-datepicker.min.js"></script>

    <script>
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: 'yyyy-mm-dd'
            });
        });

    </script>

    @yield( 'scripts')
    <title>Sports App @yield('title')</title>

</head>

@section('content')

<div class="text-center">
    <h1>{{ $user->name }}, Welcome to Click-and-Fit Dashboard! </h1>
    <h2><strong></strong></h2>
<div class="container-fluid">

@if (Session::has('error'))
    <div class="alert alert-danger">
        <p>{{Session::get('error')}}</p>
    </div>
@endif

{!! Form::open(array('url' => '/dashboard', 'class' => 'form-horizontal')) !!}
        <div class="col-md-2 col-md-offset-2">
                <div class="panel panel-default">
            <div class="panel-heading"><b>Let's Create your gymnast profile:<b></div>
              <div class="panel-body">



    {!! csrf_field() !!}

    @if (count($errors) > 0)
      <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <p>
        <div class="form-group">
        A) I am a:
        {!! Form::radio('gender','m',['class'=>'form-control']) !!} Female
        {!! Form::radio('gender','f',['class'=>'form-control']) !!} Male
        <!-- you have got to associate a value with the gender input -->
        </div>
    </p>

    <p>
                <div class="form-group">

        B) My Date of Birth is:
        <input type="text" name="age" class="datepicker" />
                </div>

    </p>

    <p>
                <div class="form-group">

        C) What are your fitness goals for the next year?<br>
        <textarea class="form-control" cols="30" rows="8" name="goals" class="form-group" maxlength="100" placeholder="Other users can see your goals." ></textarea>
                </div>


    </p> <!--default is null -->
    <p>
        D) I am better in:
        <br>{!! Form::radio('activityType','Aerobics',null,['class'=>'ActivityType']) !!}  Aerobics
        <br>{!! Form::radio('activityType','Anerobic','null',['class'=>'ActivityType']) !!} Anerobic
        <br>{!! Form::radio('activityType','both',null,['class'=>'ActivityType']) !!} I am pretty good at both
    </p>

    <p>
                        <div class="form-group">

        E) What do you expect from this app to help you?

        @foreach($expectations as $expectation)
            <br>{!! Form::checkbox('expectations[]', $expectation->id,true) !!} {!! $expectation->name !!}
        @endforeach
                        </div>

    </p>

   </div>
  <div class="panel-footer"> {!! Form::submit('Create Profile',['class'=>'form-group']) !!}</div></div>

    {!! Form::close() !!}
</div>
</div>

@endsection
@yield('content')

