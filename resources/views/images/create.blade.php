@extends('layouts.master')
@section('head')
<script src="{{ asset('assets/js/dropzone.js') }}"></script>
@endsection

@section('content')
    @if(count($errors))
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                     <li>{{ $error}}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!--  Active dropzone.js form html code. -->
    <form action="{{ url('media/show')}}" class="dropzone" id="my-awesome-dropzone" >
      {{ csrf_field() }}
    </form>
    <!-- End of Active dropzone.js form html code -->
@stop
