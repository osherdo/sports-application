@extends('layouts.master')
@section('scripts')



@section('content')
    {!! csrf_field() !!}

@if (count($errors) > 0)
  <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(Session::has('flash_message'))
            <div class="alert alert-success">
                {{ Session::get('flash_message') }}
           </div>
@endif

@if(Session::has('routine_deleted'))
<div class="alert alert-success">
  {{ Session::get('routine_deleted') }}
</div>
@endif




{{--
   You have a $user variable available
    representing the current Auth::user();
--}}

<p>Hello, {{ $user->name }}. Here's you routine list:</p>

{{--
    This user has a profile whose properties you can also display
 --}}
<!--
<p>Welcome to your active routines page: {{ $user->profile->goals }}</p>

{{--
    You can also drill down to the user's routines.
 --}}

 <p>Your Routines:</p>
<ul class="routines-list">
  @foreach($user->profile->expectations as $expectation)
    <li class="routines-list-item">{!! $expectation->name !!}</li>
  @endforeach
</ul>
-->
<div class="row">
  <div class="col-md-6 col-sm-6 col-xs-12">
<div class="list-group">

@foreach ($all_routines as $routine)
  @if ($routine->routine_name !== '')
    <div class="row exercise_row">
        <div class="col-lg-6">
            <a href="/routine_details/{{$routine->id}}" class="list-group-item">{{$routine->routine_name}}</a>
        </div>

          <div class="col-lg-2">
      <!-- When this button is clicked, we determine which routine to remove. -->
      {!! Form::open(['route' => ['deleteRoutine', $routine->id], 'method' => 'delete']) !!}
          <input type="submit" class="btn btn-warning remove_routine" style="display:inline" value="Delete">
      {!! Form::close() !!}
  </div>
    </div>

    {{-- Getting the routine id number is done through this: {{$routine->id}} , There's a placeholder for the number in routes.php. --}}
    {{-- data-token - used to attach each routine delete request a csrf token. (attached to any POST request done) --}}
@endif


<br>
@endforeach
</div>
</div>
</div>
<script src="{{asset('/js/view_routine.js')}}"></script>
<script>
window.setTimeout(function() {
    $(".alert-success").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove();
    });
}, 5000);</script>
@stop
