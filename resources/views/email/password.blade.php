<html>
<body>
<!--  this is the view that generates the email which the user will receive. it contains a variable: $token which is supplied by laravel. 
 this is the token that validates the user if he can change the password-->
<!--
 laravel creates an entry in the database in which it stores the user email and token in the same row,and then it is able to verify if the same user is accessing the token that generated it. This is done with the "password_reset" table in db.
-->
Click here to reset your password: {{ url('password/reset/'.$token) }}
</body>
</html>